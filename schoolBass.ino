



/*
Adafruit Mono 2.5W Class D Audio Amplifier
audio goes to A0

motor controller - adafruit
ain2 - arduino 9
ain1 - arduino 10
bin1 - arduino 11
bin2 - arduino 12




*/
// audioL = A2;
// audioR = A3;
//int headPin = 6;
//int bodyPin = 8;


//audio pin is A0
int RPiDataPin = 5;

int motorA2 = 9; //motor A2 is mouth
int motorA1 = 10; //motor B1 is tail
int motorC = 11; //motor B2 is neck
int motorD = 12; 

int mouthPin = motorA1;
int mouthPinGnd = motorA2;

int bodyPin = motorC;
int tailPin = motorD;

int nowTime;
int thenTime;
int talkTime;
bool alexaTalking;


void setup() {
  pinMode(mouthPinGnd, OUTPUT);
  pinMode(mouthPin, OUTPUT);

  pinMode(RPiDataPin, INPUT);
  
  pinMode(bodyPin, OUTPUT);
  pinMode(tailPin, OUTPUT);

  pinMode(LED_BUILTIN, OUTPUT);
  //analogReadResolution(12);

  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);

  digitalWrite(bodyPin, LOW); //set to low to start
  digitalWrite(mouthPin, LOW); //set to low to start  
  digitalWrite(mouthPinGnd, LOW); //set to low to start  
  digitalWrite(tailPin, LOW); //set to low to start  
}

int noiseFloor(){ //some code to find the noise floor and go a bit above
  int nowVol = analogRead(A0);
  int noiseCheck = 200;
  int samples = 50;
  
  for(int i = 0; i < samples; i++){
    if(nowVol < noiseCheck){
      nowVol += analogRead(A0);
      nowVol = nowVol / 2;
    }
  }
return nowVol + 50;
  
}

void tailFlipCheck(){
  
//should my tail be flipped?
//series of tailflips sent from the RPi could encode other instructions, read by this fish brain
  if(digitalRead(RPiDataPin) == HIGH){
        digitalWrite(tailPin, HIGH);
        digitalWrite(LED_BUILTIN, HIGH);
            
  }
  if(digitalRead(RPiDataPin) == LOW){
        digitalWrite(tailPin, LOW);
        digitalWrite(LED_BUILTIN, LOW);
            
  }
}

void loop() {

//tailFlipCheck();
//  int threshold = noiseFloor();
  int threshold = 50;
  int levelL = analogRead(A0);
  
  if(levelL > threshold){
    alexaTalking = true;
    talkTime = millis();
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(mouthPin, HIGH);
    
  //  digitalWrite(tailPin, HIGH);
    //thenTime = nowTime;
    //nowTime = millis();
    delay(100);
  }
  if(levelL <= threshold){
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(mouthPin, LOW);

  }

  nowTime = millis();
  if(nowTime > (talkTime + 2000)){
          alexaTalking = false;
          
  }

  if(alexaTalking == false){ 
    digitalWrite(mouthPin, LOW);
  }

  
 /* 
  if(nowTime > (thenTime + 1000)){
    
    digitalWrite(bodyPin, LOW);
  }
  */
  delay(4);

}
